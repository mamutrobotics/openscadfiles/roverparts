module mamutHull()
{
    difference()
    {
        union()
        {
           mamut();
            support(70, 53, 30, 0, 0, 90);
            support(70, 53, -40, 0, 0, 90);
            support(70, -115, 30, 0, 0, 90);
            support(70, -115, -40, 0, 0, 90);
            support(-70, -50, -40, 0, 0, -90);
            support(-70, -50, 30, 0, 0, -90);
            support(-70, 119, -40, 0, 0, -90);
            support(-70, 119, 30, 0, 0, -90);
        }
        supportHoles(0, 55, 145);
        supportHoles(0, -55, 145);
        supportHoles(0, -55, -145);
        supportHoles(0, 55, -145);
    }
}

module supportPin(xAxe, yAxe, zAxe)
{
    translate([ 0 + xAxe, 0 + yAxe, 0 + zAxe ]) rotate([ 0, -90, 0 ]) difference()
    {
        union()
        {
            cube([ 10, 10, 4 ]);
            translate([ 5, 0, 0 ]) cylinder(4, 5, 5, $fn = 100);
        }
        translate([ 5, 0, -0.2 ]) cylinder(4.5, 2, 2, $fn = 100);
    }
}
module support(xAxe, yAxe, zAxe, xRot, yRot, zRot)
{
    translate([ 0 + xAxe, 0 + yAxe, 0 + zAxe ]) rotate([ 0 + xRot, 0 + yRot, 0 + zRot ]) union()
    {
        supportPin();
        supportPin(70, 0, 0);
    }
}
module supportHoles(xAxe, yAxe, zAxe)
{
    fine = 100;
    radius = 4;
    hight = 15;
    rotate([ 90, 90, 0 ]) translate([ -15 + xAxe, 0 + yAxe, 0 + zAxe ]) union()
    {
        cylinder(hight, radius, radius, center = true, $fn = fine);
        translate([ 30, 0, 0 ]) cylinder(hight, radius, radius, center = true, $fn = fine);
    }
}
module mamut()
{
    difference()
    {
        cube([ 120, 300, 80 ], center = true);
        cube([ 100, 320, 60 ], center = true);
        translate([ -70, 10, -30 ]) cube([ 140, 130, 60 ]);
        translate([ -70, -140, -30 ]) cube([ 140, 130, 60 ]);
        translate([ -50, 55, 25 ]) cube([ 100, 85, 20 ]);
        translate([ -50, -40, 25 ]) cube([ 100, 85, 20 ]);
        translate([ -50, -140, 25 ]) cube([ 100, 85, 20 ]);
    }
}
module saddleJointHelper(screw_diameter)
{
    difference()
    {
        rotate([ 90, 0, 0 ]) hull()
        {
            cylinder(h = 7, d = 11.5, $fn = 500);
            translate([ 8.5, 0, 0 ]) cylinder(h = 7, d = 11.5, $fn = 500);
        }
        translate([ 4.25, 0, 3 ]) sphere(r = 5, $fn = 500);
        for(i=[-2.5,11]){
            rotate([ 90, 0, 0 ]) translate([ i, 0, -1 ]) cylinder(h = 9, d = screw_diameter, $fn = 500);
            }
    }
}
module saddleJoint()
{
    union()
    {
        rotate([ 90, 0, 0 ]) cylinder(h = 52, d = 9, $fn = 500);
        translate([ -4.5, 6, 1.2 ]) saddleJointHelper(3.2);
        translate([ 4.5, -58, 1.2 ]) rotate([ 0, 0, 180 ]) saddleJointHelper(3.2);
    }
}
module servo_support(){
    difference(){
        hull(){
            for(i=[0,10]){
                translate([i,0,0]) cylinder(h=10,d=6,$fn=500);
                }
            }
        for(i=[0,10]){
            translate([i,0,-1]) cylinder(h=12,d=3.6,$fn=100);
            }
        }
    }

module gearbox_suspension_stabilisation(){
    difference(){
        cylinder(h=14,d=6,$fn=500);
        cylinder(h=16,d=3.6,$fn=500);
    }
}

module triangle(){
    hull(){
        cube([0.1,2,7]);
        translate([2,1,0]) cylinder(d=1,h=7);
      }
  }
module rack(){
  hull(){
    cube([12,83,7]);
    for(i=[90,-7]){
      translate([6,i,0]) cylinder(d=6,h=7,$fn=100);
      }
    }
  for(i=[90,-7]){
      translate([6,i,0]) cylinder(d=6,h=14,$fn=100);
      translate([6,i,11.5]) sphere(r=4.7,$fn=100);
    }
  for(i=[0:27]){
      translate([12,i*3,0]) triangle();
    }
  }

