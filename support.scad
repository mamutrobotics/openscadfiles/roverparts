module supportPin(xAxe,yAxe,yAxe){
    translate(
        [0+xAxe,0+yAxe,0+yAxe]
        ) rotate([0,-90,0]) difference(){
        union(){
            cube([10,10,4]);
            translate([5,0,0]) cylinder(4,5,5,$fn=100);
        }
        translate([5,0,-0.2]) cylinder(4.5,2,2,$fn=100);
    }
}
module support(){
    supportPin();
    supportPin(70,0,0);
}
module supportHoles(){
    translate([-15,0,0]) union(){
    cylinder(15,4,4,center=true,$fn=100);
    translate([30,0,0]) cylinder(15,4,4,center=true,$fn=100);
    }
}
