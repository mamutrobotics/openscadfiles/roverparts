# Rover Parts
This repository contains the source code for the mesh files of our M.A.M.U.T. Rover prototype.
## mamut.scad
This is the main file that contains all the modules for compiling the different parts 
of the rover.
### mamutHull
mamutHull() represents the hull of the rover as a part, accessing many different modules 
in the process.
### supportPin
SupportPin() describes a part of the rover where the wheel suspension is mounted. It's 
implemented as a cube with half a cylinder on top and a hole in it.
### support
This module contains two support pins for the wheel suspension. It's designed to be 
attached to the wheel suspension.
### supportHoles
This method represents the holes in the chassis through which other important parts 
are connected to it.
### mamut()
This module represents the main chassis of the rover as a large rectangle from which 
several others have been cut.
### saddleJointHelper
It represents the part of the saddle joint that sits on the ball.
### saddleJoint
The rover is steered by ball and socket joints. These are the parts that sit on 
top of these balls. This module connects two of these joints together with a rod 
in between.
### servo_support()
The rover is steered by a rover that controls 2 steering rods simultaneously via 
a dedicated gearbox. This module acts as a small spacer between the gearbox and the 
servo.
### gearbox_suspension_stabilisation
This module represents a fixing of the gearbox by means of a screw that passes through 
it and is screwed to the gearbox by means of a fusion thread.
